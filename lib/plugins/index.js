const debug = require('debug')('altifalante:plugins');

module.exports = {
  notification: require('./notification'),
	beep: require('./beep'),
	email: require('./email'),
  inspector: require('./inspector')
}
