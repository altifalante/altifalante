const notification = require('./notification')
const debug = require('debug')('altifalante:inspector');
const readline = require('readline');

function inspector (configOpts, eventArgs) {
  debug(configOpts, eventArgs)
  const logger = (eventArgs && eventArgs[0]) || (configOpts && configOpts.logger);

  const rl = readline.createInterface({
    input: logger
  });

  rl.on('line', (line) => {
    const devtoolsLink = line.match(/chrome-devtools:\/\/.*/);
    debug('devtoolsLink', devtoolsLink)
    if (devtoolsLink) {
      notification({}, devtoolsLink);
    }
  });
}

module.exports = notification;
