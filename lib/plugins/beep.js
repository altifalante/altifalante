const debug = require('debug')('altifalante:beep');
const beepbeep = require('beepbeep');

function beep (configOpts, eventArgs) {
  debug(configOpts, eventArgs)
  const sound = eventArgs && eventArgs[0];
  if (sound) {
		beepbeep(sound);
  }
}

module.exports = beep;
