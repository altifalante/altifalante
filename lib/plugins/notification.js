const os = require('os');
const exec = require('child_process').exec;
const debug = require('debug')('altifalante:notification');

function notification (configOpts, eventArgs) {
  debug(configOpts, eventArgs)
  const message = (eventArgs && eventArgs[0]) || (configOpts && configOpts.message);
  if (message && os.platform() === 'darwin' ) {
    exec(`osascript -e 'display notification "${message}"'`);
		return;
  }
	if (message && os.platform() === 'freebsd' ) {
		debug('Not implemented');
		return;
  }
	if (message && os.platform() === 'linux' ) {
		debug('Not implemented');
		return;
	}
	if (message && os.platform() === 'sunos' ) {
		debug('Not implemented');
		return;
	}
	if (message && os.platform() === 'win32' ) {
		debug('Not implemented');
		return;
	}
}

module.exports = notification;
