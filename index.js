const debug = require('debug')('altifalante:lib');
const EventEmitter = require('events');
const plugins = require('./lib/plugins');

let emitterInstance = null;

const Altifalante = (config) => {
	debug('booting with', config);
  if (!emitterInstance) {
    emitterInstance = new EventEmitter();
  }

  Object.keys(config).forEach((eventKey) => {
    const event = config[eventKey];
    emitterInstance.on(eventKey, (...args) => {
      const tasks = Object.keys(event).reduce((acc, eventPluginName) => {
        const pluginOptions = event[eventPluginName];
        acc.push(plugins[eventPluginName].bind(null, pluginOptions, args));
        return acc;
      }, []);
      tasks.forEach((task) => task());
    });
  });
  return emitterInstance;
}

module.exports = Altifalante;
